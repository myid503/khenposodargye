# 法宝下载目录

## 更新课程

|标题|说明|种子|
:-|:-|:-:
|《苦乐道用》——苦乐转为道用的窍诀（全十讲）|音频| [站内](seeds/1-%E8%8B%A6%E4%B9%90%E9%81%93%E7%94%A8.torrent)-[备用](https://s3.ap-northeast-2.amazonaws.com/khenposodargye.public/seeds/1-%E8%8B%A6%E4%B9%90%E9%81%93%E7%94%A8.torrent)|
| 《佛说父母恩难报经》|音频|[站内](seeds/2-%E4%BD%9B%E8%AF%B4%E7%88%B6%E6%AF%8D%E6%81%A9%E9%9A%BE%E6%8A%A5%E7%BB%8F.torrent)-[备用](https://s3.ap-northeast-2.amazonaws.com/khenposodargye.public/seeds/2-%E4%BD%9B%E8%AF%B4%E7%88%B6%E6%AF%8D%E6%81%A9%E9%9A%BE%E6%8A%A5%E7%BB%8F.torrent)|
|《赞僧功德经》|音频|[站内](seeds/3-%E8%B5%9E%E5%83%A7%E5%8A%9F%E5%BE%B7%E7%BB%8F.torrent)-[备用](https://s3.ap-northeast-2.amazonaws.com/khenposodargye.public/seeds/3-%E8%B5%9E%E5%83%A7%E5%8A%9F%E5%BE%B7%E7%BB%8F.torrent)|

## 往期资料
往期资料正在陆续整理中，敬请关注目前正在整理的内容，前往：[法宝整理工作目录](backlog.md)

## 网络资料
了解已经收集的资料范围，前往：[网络版资料目录](netdisk.md)

## 项目介绍
阅读本项目相关说明，请前往将： [缘起](README.md)

## 推荐客户端
- bitcomet [https://www.bitcomet.com]
- qbittorrent [https://www.qbittorrent.org]
- utorrent [https://www.utorrent.com]
- free download manager [https://www.freedownloadmanager.org]
